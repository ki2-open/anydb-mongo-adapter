import { ObjectID } from "mongodb";
import { Id } from "@ki2/anydb";
import {
  normalizeId,
  objectifyId,
  remapModifiers,
  setId,
  useFilters,
  useIdKey,
  useOptions,
} from "../src";
import type { MongoAdapterOptions, IModel } from "../src";

function genOptions(
  id: string = "_id",
  disableObjectify?: boolean
): MongoAdapterOptions {
  return {
    Model: {} as IModel,
    allowedOperators: [],
    id,
    disableObjectify,
  };
}

function isObjectId(id: Id | ObjectID): id is ObjectID {
  return id instanceof ObjectID;
}

describe("test helpers function", () => {
  it("should validate useIdKey", () => {
    expect(useIdKey(genOptions())).toBe("_id");
    expect(useIdKey(genOptions("toto"))).toBe("toto");
  });

  it("should validate objectifyId", () => {
    let options = genOptions("_id", true);
    expect(objectifyId(25, options)).toBe(25);
    expect(objectifyId("123", options)).toBe("123");

    options = genOptions();
    expect(objectifyId("test", options)).toBe("test");
    expect(isObjectId(objectifyId(25, options))).toBe(true); // Could it be better to keep 25 instead of generate ObjectId ?

    const soid = new ObjectID().toHexString();
    const ooid = objectifyId(soid, options) as ObjectID;
    expect(isObjectId(ooid)).toBe(true);
    expect(ooid.toHexString()).toEqual(soid);
  });

  it("should validate normalizeId", () => {
    let options = genOptions("id");
    expect(normalizeId(null, { data: 25 }, options)).toEqual({ data: 25 });
    expect(normalizeId(0, { data: 1 }, options)).toEqual({ id: 0, data: 1 });

    options = genOptions("_id");
    expect(normalizeId(0, { _id: 0, data: 1 }, options)).toEqual({ data: 1 });
  });

  it("should validate remapModifier", () => {
    expect(remapModifiers({ $a: 0, b: 1 })).toEqual({ $a: 0, $set: { b: 1 } });
    expect(remapModifiers({ b: 1 })).toEqual({ $set: { b: 1 } });
    expect(remapModifiers({ $a: 0 })).toEqual({ $a: 0 });
    expect(remapModifiers({ b: 1, $set: { c: 2 } })).toEqual({
      $set: { b: 1, c: 2 },
    });
  });

  it("should validate setId", () => {
    let options = genOptions();
    expect(setId({ data: 0 }, options)).toEqual({ data: 0 });
    expect(setId({ _id: 0 }, options)).toEqual({ _id: 0 });

    options = genOptions("id");
    expect(setId({ id: 0 }, options)).toEqual({ id: 0 });
    const ret1 = setId({ data: 0 }, options);
    expect(ObjectID.isValid(ret1.id)).toBe(true);
  });

  it("should validate useFilters", () => {
    expect(useFilters()).toEqual({});
    expect(useFilters({ options: {} })).toEqual({});
    expect(useFilters({ filters: { skip: 25 } })).toEqual({ skip: 25 });
  });

  it("should validate useOptions", () => {
    expect(useOptions()).toEqual(undefined);
    expect(useOptions({ filters: {} })).toEqual(undefined);
    expect(useOptions({ options: { a: 1 } })).toEqual({ a: 1 });
  });
});
