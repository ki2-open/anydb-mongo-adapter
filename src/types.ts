import type { AnyDbAdapterOptions, Filters } from "@ki2/anydb";

import type { IModel } from "./model";

export interface MongoAdapterOptions extends AnyDbAdapterOptions {
  Model: IModel;
  disableObjectify?: boolean;
}

export interface MongoFilters extends Filters {
  hint?: any;
  collation?: any;
}

export { IModel };
