import { exist, extend, isArray } from "@ki2/utils";

import { AnyDbAdapter, NotFound, cleanQuery } from "@ki2/anydb";
import type { Id, Query, Params } from "@ki2/anydb";

import type { IModel, MongoAdapterOptions } from "./types";
import {
  objectifyId,
  normalizeId,
  remapModifiers,
  setId,
  useFilters,
  useOptions,
} from "./helpers";

export class MongoAdapter<T = any, D = Partial<T>> extends AnyDbAdapter<T, D> {
  constructor(options: Partial<MongoAdapterOptions>) {
    if (!exist(options.Model)) {
      throw new Error("MongoDB Model (collection) required");
    }

    super({
      id: "_id",
      ...options,
    });
  }

  get extoptions(): MongoAdapterOptions {
    return this.options as MongoAdapterOptions;
  }

  get Model(): IModel {
    return this.extoptions.Model;
  }

  set Model(model: IModel) {
    this.extoptions.Model = model;
  }

  async find(query: Query = {}, params?: Params): Promise<T[]> {
    const filters = useFilters(params);
    const options = useOptions(params);

    query = cleanQuery(query, this.options.allowedOperators);

    if (query[this.id]) {
      query[this.id] = objectifyId(query[this.id], this.extoptions);
    }

    const q = this.Model.find(query, options);

    if (filters.select) {
      let select = filters.select;
      if (isArray(select)) {
        let tmp: any = {};
        select.forEach((name) => {
          tmp[name] = 1;
        });
        select = tmp;
      }
      q.project(select);
    }

    if (filters.sort) {
      q.sort(filters.sort);
    }

    if (filters.collation) {
      q.collation(filters.collation);
    }

    if (filters.hint) {
      q.hint(filters.hint);
    }

    if (filters.limit) {
      q.limit(filters.limit);
    }

    if (filters.skip) {
      q.skip(filters.skip);
    }

    return await q.toArray();
  }

  async count(query: Query, params?: Params): Promise<number> {
    const options = useOptions(params);
    query = cleanQuery(query, this.options.allowedOperators);

    return await this.Model.count(query, options);
  }

  async get(id: Id, params?: Params): Promise<T> {
    const options = useOptions(params);

    const query: Query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    const result = await this.Model.findOne(query, options);
    if (!exist) {
      throw new NotFound(`No record found for id ${id}`);
    }
    return result;
  }

  async createOne(data: D, params?: Params): Promise<T> {
    const options = useOptions(params);
    const entry = setId(data, this.extoptions);
    const result = await this.Model.insertOne(entry, options);
    return result.ops[0];
  }

  async createMany(data: D[], params?: Params): Promise<T[]> {
    const options = useOptions(params);
    const entries = data.map((item) => setId(item, this.extoptions));
    const result = await this.Model.insertMany(entries, options);
    return result.ops;
  }

  async update(id: Id, data: D, params?: Params): Promise<T> {
    const options = useOptions(params);

    const query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    const normalizeddata = normalizeId(id, data, this.extoptions);

    await this.Model.replaceOne(query, normalizeddata, options);
    return await this.get(id, params);
  }

  async patchOne(id: Id, data: D, params?: Params): Promise<T> {
    const options = useOptions(params);
    const filters = useFilters(params);
    let query: Query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const normalizeddata = normalizeId(id, data, this.extoptions);
    const remapedModifier = remapModifiers(normalizeddata);

    await this.Model.updateOne(query, remapedModifier, options);
    return await this.get(id);
  }

  async patchMany(query: Query, data: D, params?: Params): Promise<T[]> {
    const options = useOptions(params);
    const filters = useFilters(params);

    query = cleanQuery(query, this.options.allowedOperators);

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const normalizeddata = normalizeId(null, data, this.extoptions);
    const remapedModifier = remapModifiers(normalizeddata);

    const results: any[] = await this.find(query, params);
    const ids = results.map((item) => item[this.id]);

    await this.Model.updateMany(query, remapedModifier, options);

    return await this.find({
      [this.id]: { $in: ids },
    });
  }

  async removeOne(id: Id, params?: Params): Promise<T> {
    const options = useOptions(params);
    const filters = useFilters(params);

    let query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const item = await this.get(id, params);
    await this.Model.deleteOne(query, options);
    return item;
  }

  async removeMany(query: Query, params?: Params): Promise<T[]> {
    const options = useOptions(params);
    const filters = useFilters(params);

    query = cleanQuery(query, this.options.allowedOperators);

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const items = await this.find(query, params);
    await this.Model.deleteMany(query, options);
    return items;
  }
}
