import {
  Cursor,
  InsertOneWriteOpResult,
  InsertWriteOpResult,
  UpdateWriteOpResult,
  ReplaceWriteOpResult,
  DeleteWriteOpResultObject,
} from "mongodb";

import type { Query } from "@ki2/anydb";

export interface IModel {
  count(query: Query, options: any): Promise<number>;
  find(query: Query, options: any): Cursor<any>;
  findOne(query: Query, options: any): Promise<any>;
  insertOne(data: any, options: any): Promise<InsertOneWriteOpResult<any>>;
  insertMany(data: any[], options: any): Promise<InsertWriteOpResult<any>>;
  updateOne(
    query: Query,
    data: any,
    options: any
  ): Promise<UpdateWriteOpResult>;
  updateMany(
    query: Query,
    data: any[],
    options: any
  ): Promise<UpdateWriteOpResult>;
  replaceOne(
    query: Query,
    data: any,
    options: any
  ): Promise<ReplaceWriteOpResult>;
  deleteOne(query: Query, options?: any): Promise<DeleteWriteOpResultObject>;
  deleteMany(query: Query, option: any): Promise<DeleteWriteOpResultObject>;
}
